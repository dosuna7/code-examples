#include "mcv_platform.h"
#include "comp_mayor.h"
#include "AIConstants.h"
#include "ai_manager.h"
#include "comp_skeleton.h"
#include "go_constants.h"
#include "ai_path_manager.h"
#include "comp_life.h"
#include "comp_obstacle_avoidance.h"
#include "physics_constants.h"
#include "game_object_manager.h"
#include "logic_manager.h"
#include "ui_manager.h"
#include "logic_constants.h"
#include "comp_sound_systems.h"


DECL_COMP_MANAGER( CMayor, "ia_mayor" );

void CMayor::registerMsgs( ) { 
	SUBSCRIBE( CMayor, MSG_APPEAR_MAYOR, msgWakeUp);
	SUBSCRIBE( CMayor, MSG_IA_STUNNED, stun);
}

void CMayor::setPath(int id) {
	assert(id != -1);
	path = aiPathManager.getPath(id);
}

void CMayor::msgWakeUp(const TMsg &msg) {
	WakeUp();
	owner->send(TMsg::createPhysicsMove(transform->getPosition()));
	owner->get<CSkeleton>()->changeCycle("walk");
	animation = IA_BASE::WALK;
	ChangeState("walk");

	enableHUDTimer();
}


void CMayor::stun(const TMsg &msg) {
	if (!sleeping && animation != IA_BASE::AGONY && getCurrentState().compare("dead") != 0 ) {
		string s = Random::RandomAnimation("stun", 0, 1);
		owner->get<CSkeleton>()->changeCycle(s);
		animation = IA_BASE::STUN;
		timer = msg.stun_data.stun_time;
		ChangeState("stun");
	}
}

void CMayor::scared(XMVECTOR fearPos) {
	if (!sleeping && animation != IA_BASE::STUN && animation != IA_BASE::AGONY && distEuclidean2(fearPos, owner->get<CTransform>()->getPosition()) < FEAR_AREA_SCARED_2  && getCurrentState().compare("dead") != 0 ) {
		string s = Random::RandomAnimation("stunWall", 0, 1);
		owner->get<CSkeleton>()->changeCycle(s);
		animation = IA_BASE::STUN;
		timer = TIME_FEAR_DISTANCE;
		ChangeState("stun");
	}
}


void CMayor::Init() {

	AddState("walk",		(statehandler)&CMayor::walkState);
	AddState("finalPath",	(statehandler)&CMayor::finalPathState);
	AddState("dead",		(statehandler)&CMayor::deadState);
	AddState("stun",		(statehandler)&CMayor::stunState);
	
	AddState("enter_dead_finisher",		(statehandler)&CMayor::enterFinisherDeadState);
	AddState("dead_finisher",		(statehandler)&CMayor::finisherDeadState);
	AddState("exit_dead_finisher",		(statehandler)&CMayor::exitFinisherDeadState);


	transform = owner->get<CTransform>();
	initPos = transform->getPosition();
	initQuat = transform->getRotation();

	if (has_dynamic_obstacle_avoidance)	{
		CObstacleAvoidance *oa = owner->get<CObstacleAvoidance>();
		oa->sonar_distance = 10;
	}

	acc_dt = 0;
	dead_animation_time = 7;
	shutdown_timer = IA_TIMERS::SHUTDOWN_TIMER;

	Sleep();
}


void CMayor::walkState() {
	XMVECTOR direction_to_waypoint;
	XMVECTOR final_direction;
	XMVECTOR avoidance_direction;

	XMVECTOR waypoint = path->getPosWaypoint();
	XMVECTOR path_waypoint = waypoint;
	float weight = 0;
	direction_to_waypoint = XMVector3NormalizeEst(waypoint - transform->getPosition());
	
	final_direction = direction_to_waypoint;
	final_direction = XMVector3Normalize(final_direction);
	final_direction = XMVectorSetW(final_direction, 1);
	
	XMVECTOR radians = XMVector3AngleBetweenNormals(transform->getFront(), final_direction);
	float yaw_difference = getAngleDifferenceTo(transform->getPosition(), transform->getYaw(), transform->getPosition() + final_direction);
	
	transform->rotateYaw(yaw_difference * ANGULAR_SPEED_WALK * Time.deltaTime * 2);
	transform->translate(transform->getFront() * SPEED_WALK * 1 * Time.deltaTime);
	transform->owner->send(TMsg::createPhysicsMove(transform->pos));
	
	if (arrivePoint(waypoint) && XMVector3Equal(waypoint, path_waypoint)) {
		if (path->arriveFinalPath()){
			ChangeState("finalPath");
			return;
		}
		path->nextWaypoint();
	}
	lifeWalk();
}

/**
  * Arriba al final del path (zepelins) i es desincronitza
  */
void CMayor::finalPathState() {
	owner->get<CSkeleton>()->stopCycle();
	owner->get<CSkeleton>()->playAnimation("deadIdle");
	dead = true;
	LogicManager.logicGame->forceLose();
}


void CMayor::deadState() {
	bool acabat = owner->get<CSkeleton>()->model->getMixer()->isFinishedAnimation();
	if (acabat) {
		dead = true;
		aiManager.killTarget();
		UIManager.setKilledTargetHUD(DeadTargets::TARGET_MAYOR);
		UIManager.hideTimerHUD();

		StandBy();
	}
	dissolvePerson(acabat);
}

void CMayor::stunState() {
	if (timer > 0.f) {
		timer -= Time.getDeltaTime();
	} else {
		owner->get<CSkeleton>()->changeCycle("walk");
		animation = IA_BASE::WALK;
		ChangeState("walk");
	}
	lifeIdle();
}

void CMayor::update(float dt) {
	PROFILE_FUNC();
	if (!sleeping && !dead) {
		Recalc();
	}
}

void CMayor::lifeWalk() {
	if (animation == IA_BASE::AGONY && owner->get<CSkeleton>()->getCurrentAnimation()[5] == 'I') {
		string agonyName = "agonyWalk" + string(&owner->get<CSkeleton>()->getCurrentAnimation().back());
		owner->get<CSkeleton>()->changeCycle(agonyName);
	}
	if (isInFear()) {
		float lifeBefore = owner->get<CLife>()->getLife();
		decreaseLife();
		float life = owner->get<CLife>()->getLife();

		if (life < LIFE_TO_AGONY_1) {	//Estem en agonia
			if (lifeBefore >= LIFE_TO_AGONY_1 && life < LIFE_TO_AGONY_1) {	//entro a agonia 1
				prevState = getCurrentState();
				owner->get<CSkeleton>()->changeCycle("agonyWalk1");
				animation = IA_BASE::AGONY;
			} else if (lifeBefore >= LIFE_TO_AGONY_2 && life < LIFE_TO_AGONY_2) { //entro en la segona agonia
				owner->get<CSkeleton>()->changeCycle("agonyWalk2");
			} else if (lifeBefore >= LIFE_TO_AGONY_3 && life < LIFE_TO_AGONY_3) { //entro en la tercera agonia
				owner->get<CSkeleton>()->changeCycle("agonyWalk3");
			} else if (life <= 0.f && animation != IA_BASE::DEAD) {
				owner->get<CSkeleton>()->stopCycle();
				owner->get<CSkeleton>()->playAnimation("deadWalk");
				animation = IA_BASE::DEAD;
				ChangeState("dead");
			}
		}
	} else {
		float lifeBefore = owner->get<CLife>()->getLife();
		increaseLife();
		float life = owner->get<CLife>()->getLife();
		if (animation == IA_BASE::AGONY) {
			if (life > LIFE_TO_AGONY_1) {
				owner->get<CSkeleton>()->changeCycle("walk");
				animation = IA_BASE::WALK;
			} else if (lifeBefore <= LIFE_TO_AGONY_2 && life > LIFE_TO_AGONY_2) {	
				owner->get<CSkeleton>()->changeCycle("agonyWalk1");
			} else if (lifeBefore <= LIFE_TO_AGONY_3 && life > LIFE_TO_AGONY_3) {
				owner->get<CSkeleton>()->changeCycle("agonyWalk2");
			}
		}
	}
}


void CMayor::lifeIdle() {
	if (animation == IA_BASE::AGONY && owner->get<CSkeleton>()->getCurrentAnimation()[5] == 'W') {	//ve de agonia de walk
		string agonyName = "agonyIdle" + string(&owner->get<CSkeleton>()->getCurrentAnimation().back());
		owner->get<CSkeleton>()->changeCycle(agonyName);
	}
	if (isInFear()) {
		float lifeBefore = owner->get<CLife>()->getLife();
		decreaseLife();
		float life = owner->get<CLife>()->getLife();

		if (life < LIFE_TO_AGONY_1) {
			if (lifeBefore >= LIFE_TO_AGONY_VAG_1 && life < LIFE_TO_AGONY_VAG_1) {	
				prevState = getCurrentState();
				owner->get<CSkeleton>()->changeCycle("agonyIdle1");
				animation = IA_BASE::AGONY;
			} else if (lifeBefore >= LIFE_TO_AGONY_VAG_2 && life < LIFE_TO_AGONY_VAG_2) { 
				owner->get<CSkeleton>()->changeCycle("agonyIdle2");
			} else if (lifeBefore >= LIFE_TO_AGONY_VAG_3 && life < LIFE_TO_AGONY_VAG_3) { 
				owner->get<CSkeleton>()->changeCycle("agonyIdle3");
			} else if (life <= 0.f && animation != IA_BASE::DEAD) {
				owner->get<CSkeleton>()->stopCycle();
				owner->get<CSkeleton>()->playAnimation("deadIdle");
				animation = IA_BASE::DEAD;
				ChangeState("dead");
			} 
		}
	} else {
		float lifeBefore = owner->get<CLife>()->getLife();
		increaseLife();
		float life = owner->get<CLife>()->getLife();
		if (animation == IA_BASE::AGONY) {
			if (life > LIFE_TO_AGONY_1) {
				string s = Random::RandomAnimation("stun", 0, 1);
				owner->get<CSkeleton>()->changeCycle(s);
				animation = IA_BASE::STUN;
			} else if (lifeBefore <= LIFE_TO_AGONY_2 && life > LIFE_TO_AGONY_2) {	
				owner->get<CSkeleton>()->changeCycle("agonyIdle1");
			} else if (lifeBefore <= LIFE_TO_AGONY_3 && life > LIFE_TO_AGONY_3) {
				owner->get<CSkeleton>()->changeCycle("agonyIdle2");
			}
		}
	}
}

/*
 *Finisher
 */
void CMayor::enterFinisherDeadState() {
	owner->get<CSkeleton>()->stopCycle();
	owner->get<CSkeleton>()->playAnimation("finisher_death", true);
	ChangeState("dead_finisher");
	acc_dt = 0;
	UIManager.hideTimerHUD();
	owner->get<CCompSoundSystems>()->play("Estrangular");
}

void CMayor::finisherDeadState() {
	acc_dt += Time.deltaTime;
	if (acc_dt > dead_animation_time)	{
		ChangeState("exit_dead_finisher");
		acc_dt = 0;
		owner->send(TMsg::createDissolveMSG(0.5));
		owner->setCullingState(false);
		UIManager.setKilledTargetHUD(DeadTargets::TARGET_MAYOR + 1);
	}

	CTransform *myTransform = owner->get<CTransform>();
	XMVECTOR target_position = aiManager.getPosPlayer();
	float yaw_difference = getAngleDifferenceTo(myTransform->getPosition(), myTransform->getYaw(), target_position);
	myTransform->rotateYaw(yaw_difference * deg2rad(90) * Time.deltaTime * 5);
}

void CMayor::exitFinisherDeadState() {
	acc_dt += Time.deltaTime;
	if (acc_dt > shutdown_timer) {
		dead = true;

		aiManager.killTarget();
		owner->setCullingState(false);
		StandBy();
	}
}

void CMayor::restore() {
	owner->get<CTransform>()->setPosition(initPos);
	owner->get<CTransform>()->setRotation(initQuat);
	owner->get<CLife>()->resetLife();
	path->setCurrent(1);
	path->setIncrement(true);

	WakeUp();
	owner->send(TMsg::createPhysicsMove(transform->getPosition()));
	owner->get<CSkeleton>()->changeCycle("walk");
	animation = IA_BASE::WALK;
	ChangeState("walk");
	acc_dt = 0;
}

void CMayor::enableHUDTimer() {
	UIManager.setTimerHUD(hud_timer);
	UIManager.showTimerHUD();
	UIManager.setTargetToKillHUD((int)DeadTargets::TARGET_MAYOR + 1);
}
