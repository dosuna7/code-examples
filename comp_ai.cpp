#include "mcv_platform.h"
#include "comp_ai.h"
#include "comp_skeleton.h"
#include "AIConstants.h"
#include "ai_manager.h"
#include "comp_fear.h"
#include "comp_life.h"
#include "aic_player.h"
#include "go_constants.h"
#include "comp_fear_victim.h"
#include "physics_manager.h"
#include "rendering_manager.h"
#include "comp_dissolve.h"

IA_BASE::IA_BASE():losingLife(false), dead(false), sleeping(false) {
	has_dynamic_obstacle_avoidance = false;
	has_navmesh_agent = false;
	shutdown_timer = 1.f;
	after_dead = false;
	dead_animation_time = 3;
	acc_dt = 0;
}

void IA_BASE::WakeUp(){ 
	owner->send(MSG_ENTITY_WAKEUP);
	owner->send(MSG_VISIBLE);
	sleeping = false;
	dead = false;
	after_dead = false;
	acc_dt = 0;
}

void IA_BASE::Sleep(){
	owner->send(MSG_INVISIBLE);
	owner->send(MSG_ENTITY_SLEEP);
	sleeping = true;
	if (owner->has<CFearVictim>()){
		owner->get<CFearVictim>()->sleep();
	}
}

bool IA_BASE::isSleeping(){
	return sleeping;
}

	
void IA_BASE::StandBy(){
	Sleep();
	owner->send(TMsg::createPhysicsMove(XMVectorSet(-200.f, -10.f, -200.f, 0.f)));
	owner->get<CTransform>()->setPosition(XMVectorSet(-200.f, -10.f, -200.f, 0.f));
}


void IA_BASE::advanceWalk(){
	CTransform *t = owner->get<CTransform>();
	t->setPosition(t->getPosition() + t->getFront()*SPEED_WALK*Time.getDeltaTime());
	owner->send(TMsg::createPhysicsMove(t->pos));
}

void IA_BASE::advanceRun(){ 
	CTransform *t = owner->get<CTransform>();
	t->setPosition(t->getPosition() + t->getFront()*SPEED_RUN*Time.getDeltaTime());
	owner->send(TMsg::createPhysicsMove(t->pos));
}

void IA_BASE::advanceToPoint(float linearSpeed, float angularSpeed, XMVECTOR destiny) { 
	CTransform *t = owner->get<CTransform>();
	t->setPosition(t->getPosition() + t->getFront()*linearSpeed*Time.getDeltaTime());
	t->setYaw(t->getYaw() + t->getAngleTo(destiny)*angularSpeed * Time.getDeltaTime());
	t->owner->send(TMsg::createPhysicsMove(t->pos));
}

void IA_BASE::turnTo(XMVECTOR pos){ 
	owner->get<CTransform>()->rotateYaw(owner->get<CTransform>()->getAngleTo(pos));
}
void IA_BASE::turnAngle(float yaw) { 
	owner->get<CTransform>()->rotateYaw(yaw);
}

bool IA_BASE::arrivePoint(XMVECTOR distiny) {
	return distEuclidean2(owner->get<CTransform>()->getPosition(), distiny) < 0.08f;
}

float IA_BASE::dist2PlayerSq() {
	return distEuclidean2(owner->get<CTransform>()->getPosition(), aiManager.getPosPlayer());
}

bool IA_BASE::isInFear() {
	XMVECTOR pIA = owner->get<CTransform>()->getPosition();
	float distToPlayer = distEuclidean2(pIA, aiManager.getPosPlayer());
	if (owner->has<CFearVictim>()){
		unsigned state = owner->get<CFearVictim>()->checkFearSq(distToPlayer, isAgonizing());
		return owner->get<CFearVictim>()->isDangerState(state);
	}else{
		float radiusDamagePlayer = aiManager.getPlayer()->go_player->get<CFear>()->getRadiusDamage();
		return distToPlayer < radiusDamagePlayer*radiusDamagePlayer;
	}
}

void IA_BASE::checkOnEnterInFear() {
	if (!losingLife) {
		losingLife = true;
	}
}

void IA_BASE::checkOnExitInFear() {
	if (losingLife){
		losingLife = false;
	}
}


void IA_BASE::decreaseLife(){
	float distToPlayer = distEuclidean2(owner->get<CTransform>()->getPosition(), aiManager.getPosPlayer());
	owner->get<CLife>()->decreaseLive(sqrtf(distToPlayer));
}

void IA_BASE::increaseLife() {
	owner->get<CLife>()->increaseLife();
}

bool IA_BASE::isAgonizing() {
	float life = owner->get<CLife>()->getLife();
	return life < LIFE_TO_AGONY && life > 0.f;
}

bool IA_BASE::isActive() {
	return !sleeping & !dead;
}

void IA_BASE::dissolvePerson(bool finished){
	if (owner->has<CDissolve>()){
		if (finished){
			owner->get<CDissolve>()->stopTimer();
		} else {
			if (owner->get<CSkeleton>()->isAnimationAt("deadIdle", RenderManager.peopleDeathSync*2.7f)){
				owner->get<CDissolve>()->startTimer(RenderManager.peopleDeathSpeed*2.7);
			}else if(owner->get<CSkeleton>()->isAnimationAt("deadWalk", RenderManager.peopleDeathSync)){
				owner->get<CDissolve>()->startTimer(RenderManager.peopleDeathSpeed);
			}else if (owner->get<CSkeleton>()->isAnimationAt("finisher_death", RenderManager.peopleDeathSync)) {
				owner->get<CDissolve>()->startTimer(RenderManager.peopleDeathSpeed);
			}
		}
	}
}

XMVECTOR IA_BASE::getCollingPos(float timeRun) {
	CTransform *t = owner->get<CTransform>();
	float distance = timeRun*SPEED_RUN - 0.5f;
	XMVECTOR center = t->getPosition() + XMVectorSet(0.f, 0.4f, 0.f, 0.f);
	XMVECTOR centerDest = center + distance * t->getFront();
	CPhysicsManager::RaycastHit hitCenter;

	bool collisionCenter = PhysicsManager.Raycast(center, centerDest, Mathf::Vector3Magnitude(center, centerDest), hitCenter, PhysicsConstants::BUILDINGS);
	if (collisionCenter)
		return( hitCenter.position - 0.5f*t->getFront());
	return XMVectorSet(0.f, 0.f, 0.f, -1.f);
}
