#ifndef INC_COMPONENT_IA_H
#define INC_COMPONENT_IA_H

#include "mcv_platform.h"
#include "component.h"


class IA_BASE: public CComponent{
protected:
	bool losingLife;
	bool dead;
	bool after_dead;
	bool sleeping;
	
	int animation;

	float acc_dt;
	float dead_animation_time;
	float shutdown_timer;
	float stunned_time;
	float stun_time;

public:
	bool has_navmesh_agent;
	bool has_dynamic_obstacle_avoidance;

	IA_BASE();

	void WakeUp();
	void Sleep();
	void StandBy();
	void advanceWalk();
	void advanceRun();
	void advanceToPoint(float linearSpeed, float angularSpeed, XMVECTOR destiny);
	void turnTo(XMVECTOR pos);
	void turnAngle(float rad);
	bool arrivePoint(XMVECTOR distiny);
	float dist2PlayerSq();
	bool isInFear();
	void checkOnEnterInFear();
	void checkOnExitInFear();
	void decreaseLife();
	void increaseLife();
	bool isAgonizing();
	bool isActive();
	bool isSleeping();
	void dissolvePerson(bool finished);
	XMVECTOR getCollingPos(float timeRun);

	enum Animation {
		IDLE,
		WALK,
		TURN,
		RETURN_BORN_POS,
		BEGIN_RUN,
		RUN,
		STOP,
		STUN,
		GO_DISTRACTION,
		IDLE_DISTRACTION,
		AGONY,
		CHASE,
		DEAD,
		DEAD_FEAR,
		TALK,
		KNOCKING_DOOR
	};

};




#endif