#ifndef INC_COMP_MAYOR_H
#define INC_COMP_MAYOR_H

#include "mcv_platform.h"
#include "comp_ai_sm.h"
#include "comp_transform.h"

class Path;

class CMayor: public AI_SM {

	float timer;
	float hud_timer;

	Path *path;
	CTransform *transform;

	XMVECTOR initPos;
	XMVECTOR initQuat;

	//Actions
	void walkState();
	void finalPathState();
	void deadState();
	void stunState();

	void lifeWalk();
	void lifeIdle();

	void enterFinisherDeadState();
	void finisherDeadState();
	void exitFinisherDeadState();

	void msgWakeUp(const TMsg &msg);
	void stun(const TMsg &msg);


public:
	CMayor() { }

	void Init();
	void update(float dt);
	void setPath(int id);
	void restore();
	void setHudTimer(int seconds) {hud_timer = seconds;};
	int getHudTimer() {return hud_timer;};
	void enableHUDTimer();

	static void registerMsgs();

	void scared(XMVECTOR fearPos);

};




#endif