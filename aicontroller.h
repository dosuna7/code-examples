#ifndef _AICONTROLLER_H
#define _AICONTROLLER_H

// ai controllers using maps to function pointers for easy access and scalability. 
// we put this here so you can assert from all controllers without including everywhere
#include <assert.h>	
#include <string>
#include <map>
using namespace std;

// states are a map to member function pointers, to 
// be defined on a derived class. 
class AIController;

typedef void (AIController::*statehandler)(); 

class AIController{
	// the states, as maps to functions
	string state;
	map<string,statehandler>statemap;

	public:
		
		void ChangeState(string);	// state we wish to go to //pregunta si existeix aquest estat??
		virtual void Init();	// resets the controller	//inicialitza, els fills ho implementen
		void Recalc();	// recompute behaviour
		void AddState(string,statehandler);	//s'inserta en el mapa 
		bool isInState(string searchState);	// Devuelve true si se encuentra en el estado state
		bool existState(string searchState); // Devuelve true si existe el estado
		string getCurrentState();	// Devuelve true si se encuentra en el estado state

	};

#endif