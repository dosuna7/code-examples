#include "mcv_platform.h"
#include "comp_ai_sm.h"
#include "comp_life.h"
#include "comp_skeleton.h"
#include "go_constants.h"
#include "ai_manager.h"



AI_SM::AI_SM() { }

void AI_SM::life() {
	if (isInFear()) {
		float lifeBefore = owner->get<CLife>()->getLife();
		decreaseLife();
		float life = owner->get<CLife>()->getLife();
		if (isAgonizing() && lifeBefore >= LIFE_TO_AGONY) {
			prevState = getCurrentState();
			ChangeState("agony");
		} else if (life <= 0.f && animation != IA_BASE::DEAD) {
			owner->get<CSkeleton>()->stopCycle();
			owner->get<CSkeleton>()->playAnimation("dead");
			animation = IA_BASE::DEAD;
			ChangeState("dead");
		}
	} else {
		increaseLife();
	}
}

