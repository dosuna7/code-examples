#ifndef INC_COMP_CARDENAL_H
#define INC_COMP_CARDENAL_H

#include "mcv_platform.h"
#include "comp_ai_sm.h"

class Path;

class CCardenal: public AI_SM
{
	float acc_dt;
	float timer;
	float timerQTE;
	float hud_timer;

	bool stunned;
	bool chase;
	bool was_chasing;
	bool return_born_pos;

	// Comps
	CTransform *transform;

	Path *path;

	XMVECTOR initPos;
	XMVECTOR initQuat;

	//Actions
	void speakState();
	void walkState();
	void stunState();
	void endStunState();
	void finalPathState();
	void qteState();
	void chaseState();
	void enterKnockingDoorState();

	void enterFinisherDeadState();
	void finisherDeadState();
	void exitFinisherDeadState();

	void goChurch(const TMsg &msg);
	void stun(const TMsg &msg);
	void msgWakeUp(const TMsg &msg);
	void onIAChase(const TMsg &msg);
	void onIACancelChase(const TMsg &msg);

public:

	CCardenal() { }

	void Init();
	void update(float dt);
	void restore();
	void setPath(int id);
	void setHudTimer(int seconds) {hud_timer = seconds;};
	int getHudTimer() {return hud_timer;};



	static void registerMsgs();

};




#endif