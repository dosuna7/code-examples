#include "mcv_platform.h"
#include "aicontroller.h"

void AIController::Init() {
}


void AIController::Recalc(){
	// this is a trusted jump as we've tested for coherence in ChangeState
	(this->*statemap[state])();
}


void AIController::ChangeState(std::string newstate){
	// try to find a state with the suitable name
	if (statemap.find(newstate) == statemap.end()) //Si no existeix peta, perquè hi hauria de ser
	{
		// the state we wish to jump to does not exist. we abort
		exit(-1);
	}
	state=newstate;	//Si existeix canvia l'estat al nou estat
}

bool AIController::existState(string searchState){
	return statemap.find(searchState) != statemap.end();
}

void AIController::AddState(std::string name, statehandler sh){
	// try to find a state with the suitable name
	if (statemap.find(name) != statemap.end())		//Si l'estat ja existeix peta
	{
		// the state we wish to jump to does exist. we abort
		exit(-1);
	}
	statemap[name]=sh;	//Si no existeix l'estat l'afegim
}


bool AIController::isInState(string searchState){
	return state == searchState;
}

string AIController::getCurrentState() {
	return state; 
}

