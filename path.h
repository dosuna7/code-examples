#ifndef INC_COMP_WAYPOINT_H
#define INC_COMP_WAYPOINT_H

#include "mcv_platform.h"

struct Waypoint {
	XMVECTOR quat;			// Quaterni� que guarda la rotaci� que volem que tingui en el waypoint
	XMVECTOR pos;			// Posici� del waypoint
	float time;				// Temps que volem que quedi parat (0 si no volem que pari)

	Waypoint(XMVECTOR quat, XMVECTOR pos, float time);

};

class Path {
	int id;
	bool cyclic;
	bool incrementar;
	int current;
	vector <Waypoint> waypoints;

public:
	Path():current(0),cyclic(false),incrementar(true) { }
	Path(int id, bool cyclic);

	void addWaypoint(Waypoint w) { waypoints.push_back(w); }

	XMVECTOR getPosWaypoint() { return waypoints[current].pos; }
	float getTimeWaypoint() { return waypoints[current].time; }
	XMVECTOR getOrientation() { return waypoints[current].quat; }
	int getCurrent() { return current; }
	XMVECTOR startPath();
	void setCurrent(int numWaypoint);
	void nextWaypoint();
	bool arriveFinalPath();
	bool isExtreme();
	void changeIncrement();
	void setIncrement(bool increment);

};

#endif