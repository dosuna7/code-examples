#include "mcv_platform.h"

#include "ai_manager.h"
#include "material_manager.h"
#include "AIConstants.h"
#include "dbg_manager.h"
#include "physics_manager.h"
#include "angular.h"
#include "comp_transform.h"
#include "comp_navmesh_agent.h"
#include <time.h>
#include "aic_player.h"
#include "game_object.h"
#include "comp_fear.h"
#include "camera_manager.h"
#include "game_object_parser.h"
#include "XMLParser.h"
#include "street_graph.h"
#include "game_object_manager.h"
#include "debug_ui.h"
#include <algorithm>
#include "comp_physics_dynamic.h"
#include "postprocess_manager.h"
#include "rendering_manager.h"
#include "comp_renderable.h"
#include "comp_obstacle_avoidance.h"
#include "comp_skeleton.h"
#include "logic_constants.h"
#include "logic_manager.h"
#include "comp_trigger_sphere.h"
#include "ui_manager.h"

//IAS
#include "comp_vagabondFire.h"
#include "comp_guardian.h"
#include "comp_walker.h"
#include "comp_talker.h"
#include "comp_woman_priest.h"
#include "comp_people_market.h"
#include "comp_drunk.h"
#include "comp_mayor.h"
#include "comp_observator.h"
#include "comp_cardenal.h"
#include "comp_banker.h"
#include "comp_vagabond.h"
#include "comp_obelisc.h"
#include "comp_walkerRandom.h"

#include <random>
#include <functional>


AIManager aiManager;

AIManager::AIManager() {
	initialized = false;
}

void cargarXMLs() {
	GameObjectParser parser;
	bool xml_ok = parser.xmlParseFile( "data/death.xml" );
	assert(xml_ok || fatal( "Error parsing death xml : %s\n", parser.getXMLError().c_str() ));
	xml_ok = parser.xmlParseFile( "data/people/textures.xml" );
	assert( xml_ok || fatal( "Error parsing people textures  xml : %s\n", parser.getXMLError().c_str() ));
	xml_ok = parser.xmlParseFile( "data/people/observator.xml" );
	assert( xml_ok || fatal( "Error parsing observator xml : %s\n", parser.getXMLError().c_str() ));
	xml_ok = parser.xmlParseFile( "data/people/targets.xml" );
	assert( xml_ok || fatal( "Error parsing targets xml : %s\n", parser.getXMLError().c_str() ));
	xml_ok = parser.xmlParseFile( "data/people/talkers.xml" );
	assert( xml_ok || fatal( "Error parsing talkers xml : %s\n", parser.getXMLError().c_str() ));
	xml_ok = parser.xmlParseFile( "data/people/vagabondFire.xml" );
	assert( xml_ok || fatal( "Error parsing vagabondFire xml : %s\n", parser.getXMLError().c_str() ));
	xml_ok = parser.xmlParseFile( "data/people/vagabondFloor.xml" );
	assert( xml_ok || fatal( "Error parsing vagabondFloor.xml : %s\n", parser.getXMLError().c_str() ));
	xml_ok = parser.xmlParseFile( "data/people/guardians.xml" );
	assert( xml_ok || fatal( "Error parsing guardians.xml : %s\n", parser.getXMLError().c_str() ));
	xml_ok = parser.xmlParseFile( "data/people/people_market.xml" );
	assert( xml_ok || fatal( "Error parsing people_market.xml : %s\n", parser.getXMLError().c_str() ));
	xml_ok = parser.xmlParseFile( "data/animated_objects.xml" );
	assert( xml_ok || fatal( "Error parsing animated objects xml : %s\n", parser.getXMLError().c_str() ));
}

void AIManager::initSBB() {
	//Init SBB
	sbb["chase"] = 0;
	sbb["stun"] = 0;
}

void AIManager::Init()
{
	srand((unsigned)time(0)); 

	// Load the file maps
	town.InitTown();

	// Create AI to the player
	aiPlayer = new AICPlayer;
	curTarget = 0;
	deadPeople = 0;

	// Load xmls 
	cargarXMLs();
	createRandomWalker(MAX_WALKERS);
	injectPeopleInZone(aiPlayer->curZone);

	posObservation.reserve(NUM_OBSERVATORS+1);
	timeFear = 0.f;
	initialized = true;
}

/**
  * Create a max number of walkers
  */
void AIManager::createRandomWalker(int maxWalkers) {
	GameObjectParser parser;
	bool xml_ok;
	string name = "walkerRandom";
	string cur_name;
	GameObject *go;

	int women_counter = 0;
	int women_blue_counter = 0;
	int men_counter = 0;

	//Meshes
	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution1(0,WALKER_TYPES_COUNT - 1);
	auto mesh_dice = std::bind( distribution1, generator );

	// Textures
	std::uniform_int_distribution<int> distribution2(0,2);
	auto material_dice = std::bind( distribution2, generator );
	int material_random = 0;

	int woman_random = 0;

	for (int i = 0; i < maxWalkers; i++) 
	{
		int random = mesh_dice();
		switch(random)
		{
			case MAN:
			case MAN_HAT:
			case MAN_CHUBY:
				xml_ok = parser.xmlParseFile( "data/people/randomWalker.xml" );
				assert(xml_ok || fatal( "Error parsing randomWalker xml : %s\n", parser.getXMLError().c_str() ));
				go = GameObjectManager.get("walkerRandom");
				material_random = material_dice();
				switch(material_random)
				{
					case 0: 
						go->get<CRenderable>()->changeMaterialByName("walker_skel", 0);
						break;
					case 1:
						go->get<CRenderable>()->changeMaterialByName("walker_skel_2", 0);
						break;
					default:
						break;
				}
				break;

			case WOMAN_GENERIC:
				xml_ok = parser.xmlParseFile( "data/people/prefab_WomanGeneric.xml" );
				assert(xml_ok || fatal( "Error parsing randomWalker xml : %s\n", parser.getXMLError().c_str() ));
				go = GameObjectManager.get("walkerRandom");
				material_random = woman_random;
				switch(material_random)
				{
					case 0: 
						go->get<CRenderable>()->changeMaterialByName("lady_generica_skel_blau", 0);
						break;
					case 1:
						go->get<CRenderable>()->changeMaterialByName("lady_generica_skel_verd", 0);
						break;
					case 2:
						go->get<CRenderable>()->changeMaterialByName("lady_generica_skel_lila", 0);
						break;
					default:
						break;
				}

				++woman_random;
				break;
			case WOMAN_CHUBY:
				xml_ok = parser.xmlParseFile( "data/people/prefab_WomanChuby.xml" );
				assert(xml_ok || fatal( "Error parsing randomWalker xml : %s\n", parser.getXMLError().c_str() ));
				go = GameObjectManager.get("walkerRandom");

				//material_random = material_dice();
				material_random = woman_random;
				switch(material_random)
				{
					case 0: 
						go->get<CRenderable>()->changeMaterialByName("lady_grassoneta_skel_blau", 0);
						break;
					case 1:
						go->get<CRenderable>()->changeMaterialByName("lady_grassoneta_skel_verd", 0);
						break;
					case 2:
						go->get<CRenderable>()->changeMaterialByName("lady_grassoneta_skel_lila", 0);
						break;
					default:
						break;
				}
				++woman_random;
				break;
			default:
				break;
		}
		cur_name = go->getName();
		name += int2string(i);
		go->setName(name.c_str());
		go->get<CPhysicsDynamic>()->changeOwnerName();
		GameObjectManager.changeName(cur_name, name);
		woman_random = woman_random%3;
	}
}



AICPlayer * AIManager::getPlayer() {
	return aiPlayer;
}

/**
  * Get position to player
  */
XMVECTOR AIManager::getPosPlayer() {
	return aiPlayer->go_player->get<CTransform>()->getPosition();
}

int AIManager::getZonePlayer() { 
	return aiPlayer->curZone; 
}

/**
  * Get the current target
  */
GameObject* AIManager::getCurrentTarget() {
	if (curTarget == TARGET_BANKER) return ias["banker"][0];
	else if (curTarget == TARGET_MAYOR) return ias["mayor"][0];
	else if (curTarget == TARGET_CARDINAL) return ias["cardenal"][0];
	else return NULL;
}

void AIManager::createAIPlayer(GameObject *goPlayer) {
	aiPlayer->go_player = goPlayer;
	aiPlayer->thirdCamera = &CameraManager.getPlayerCamera();
	aiPlayer->Init();
}

void AIManager::debugInfoIAToolbar() {
	int numWalkers = 0;
	for (int i = 0; i < ias["walkerRandom"].size(); i++) {
		if (!ias["walkerRandom"][i]->get<CWalkerRandom>()->isSleeping())
			numWalkers++;
	}
	DebugManager.debugUI->debugInfo.numWalkers = numWalkers;
	DebugManager.debugUI->debugInfo.idZone = aiPlayer->curZone;
}

void AIManager::recalc() {
	PROFILE_FUNC();

	double timer = Time.now();

	if (initialized) {
		debugInfoIAToolbar();
		if (timeFear != 0.f) {
			if (timeFear > 0.06f) {
				infuseFear(posFear);
				timeFear -= Time.getDeltaTime();
			} else if (timeFear > 0.f) {	//Fear acabado
				desactivateFear();
				timeFear = 0.f;
			}
		}
	}
}

void AIManager::infuseFear(const XMVECTOR &fearPos) {
	for (int i = 0; i < ias["observator"].size(); i++) {
		ias["observator"][i]->get<CObservator>()->activateScared(fearPos);
	}
	for (int i = 0; i < ias["market"].size(); i++) {
		ias["market"][i]->get<CPeopleMarket>()->activateScared(fearPos);
	}
	for (int i = 0; i < ias["talkers"].size(); i++) {
		ias["talkers"][i]->get<CTalker>()->activateScared(fearPos);
	}
	GameObjectManager.get("WomanPriest")->get<CWomanPriest>()->scared(fearPos);
	GameObjectManager.get("Banker")->get<CBanker>()->scared(fearPos);
	GameObjectManager.get("Mayor")->get<CMayor>()->scared(fearPos);
	for (int i = 0; i < ias["vagabondFire"].size(); i++) {
		ias["vagabondFire"][i]->get<CVagabondFire>()->activateScared(fearPos);
	}
	for (int i = 0; i < ias["vagabondFloor"].size(); i++) {
		ias["vagabondFloor"][i]->get<CVagabond>()->activateScared(fearPos);
	}
	for (int i = 0; i < ias["walkerRandom"].size(); i++) {
		ias["walkerRandom"][i]->get<CWalkerRandom>()->activateScared(timeFear, fearPos);
	}
}

void AIManager::desactivateFear() {
	for (int i = 0; i < ias["walkerRandom"].size(); i++) {
		ias["walkerRandom"][i]->get<CWalkerRandom>()->disableScared();
	}
}

void AIManager::activatePlayerFear() {
	if (timeFear == 0.f) {
		timeFear = TIME_FEAR_PLAYER;
		posFear = getPosPlayer();
	}
}

void AIManager::activateDistanceFear(XMVECTOR positionFear) {
	if (timeFear == 0.f) {
		timeFear = TIME_FEAR_DISTANCE;
		posFear = positionFear;
	}
}

void AIManager::restartAIParams(){
	curTarget = 0;
	deadPeople = 0;
}

bool AIManager::killTarget() {
	if (curTarget == TARGET_BANKER) {
		LogicManager.onTargetDead(TARGET_BANKER);
		curTarget++;
		return true;
	}

	if (curTarget == TARGET_MAYOR) {
		LogicManager.onTargetDead(TARGET_MAYOR);
		curTarget++;
		return true;
	}

	if (curTarget == TARGET_CARDINAL) {
		LogicManager.onTargetDead(TARGET_CARDINAL);
		curTarget++;
		return true;
	}
	return false;
}

bool AIManager::finishTarget() {
	if (curTarget == TARGET_BANKER)	{
		CBanker *the_target = ias["banker"][0]->get<CBanker>();
		the_target->ChangeState("enter_dead_finisher");
		if (the_target->owner->has<CSphereTrigger>()) {
			the_target->owner->get<CSphereTrigger>()->exitTrigger();
			the_target->owner->get<CSphereTrigger>()->enabled = false;
		}
		return true;
	}

	if (curTarget == TARGET_MAYOR)	{
		CMayor *the_target = ias["mayor"][0]->get<CMayor>();
		the_target->ChangeState("enter_dead_finisher");
		if (the_target->owner->has<CSphereTrigger>()){
			the_target->owner->get<CSphereTrigger>()->exitTrigger();
			the_target->owner->get<CSphereTrigger>()->enabled = false;
		}
		return true;
	}

	if (curTarget == TARGET_CARDINAL)	{
		CCardenal *the_target = ias["cardenal"][0]->get<CCardenal>();
		the_target->ChangeState("enter_dead_finisher");
		if (the_target->owner->has<CSphereTrigger>()){
			the_target->owner->get<CSphereTrigger>()->exitTrigger();
			the_target->owner->get<CSphereTrigger>()->enabled = false;
		}
		return true;
	}

	usePlayerQTE(false);

	return false;
}



bool AIManager::isTargetAtFinisherDistance() {
	XMVECTOR player_pos = aiPlayer->playerTransform->getPosition();
	if (curTarget == TARGET_CARDINAL && ias["cardenal"][0]->get<CCardenal>()->getCurrentState().compare("enter_knocking_door") == 0) {
		CCardenal *the_target = ias["cardenal"][0]->get<CCardenal>();
		if (the_target->getCurrentState().compare("enter_knocking_door") == 0){
			return true;
		}
	}

	if (curTarget == TARGET_BANKER)	{
		CBanker *the_target = ias["banker"][0]->get<CBanker>();
		if (Mathf::Vector3Magnitude(player_pos, the_target->owner->get<CTransform>()->getPosition()) < IA_DISTANCES::FINISHER_DISTANCE){
			return true;
		}	
	}

	if (curTarget == TARGET_MAYOR)	{
		CMayor *the_target = ias["mayor"][0]->get<CMayor>();
		if (Mathf::Vector3Magnitude(player_pos, the_target->owner->get<CTransform>()->getPosition()) < IA_DISTANCES::FINISHER_DISTANCE)	{
			return true;
		}	
	}

	return false;
}


vector<GameObject *> AIManager::getIAByType(string type) {
	return ias[type];
}

/**
  * Injecta random Walkers in street
  * This function is for testing
  */
void AIManager::injectWalker() {
	PROFILE_FUNC();
	for (int i = 0; i < ias["walkerRandom"].size(); i++) {
		GameObject *go = ias["walkerRandom"][i];
		if (go->get<CWalkerRandom>()->isSleeping()) {
			go->get<CWalkerRandom>()->reappear();
			XMVECTOR pos;
			XMVECTOR vDir;
			Street *street = NULL;
			Crossing *cross = NULL;
			Track *track = NULL;
			town.getRandomPos(go, pos, vDir, &cross, &street, &track);
			go->get<CTransform>()->setPosition(pos);
			go->get<CTransform>()->setYaw(atan2(XMVectorGetX(vDir), XMVectorGetZ(vDir)));
			go->get<CWalkerRandom>()->setCross(cross);
			go->get<CWalkerRandom>()->setStreet(street);
			go->get<CWalkerRandom>()->setTrack(track);
			break;
		}
	}
}

void AIManager::intelligenceInjection(int prevZone, int curZone) {
	vector<int> newIds;
	vector<int> prevIds;

	// Save de Id's to the street
	for (int i = 0; i < town.zones[curZone]->linkedStreets.size(); i++)	
		newIds.push_back(town.zones[curZone]->linkedStreets[i]->getId());
	for (int i = 0; i < town.zones[prevZone]->linkedStreets.size(); i++)	
		prevIds.push_back(town.zones[prevZone]->linkedStreets[i]->getId());
	//Sorting the streets
	sort (newIds.begin(), newIds.end());
	sort (prevIds.begin(), prevIds.end());
	//Analitze the difference between streets
	int itNew = 0;
	int itPrev = 0;
	while (itNew < newIds.size() && itPrev < prevIds.size()) {
		if (prevIds[itPrev] < newIds[itNew]) {
			town.sleepAllToStreet(prevIds[itPrev]);
			++itPrev;
		} else if (prevIds[itPrev] > newIds[itNew]) {
			town.wakeUpStreet(newIds[itNew]);
			++itNew;
		} else {
			++itNew;
			++itPrev;
		}
	}
	
	for (; itNew < newIds.size(); itNew++) {
		int x = itNew;
		town.wakeUpStreet(newIds[itNew]);
	}
	for (; itPrev < prevIds.size(); itPrev++) {
		int x = itPrev;
		town.sleepAllToStreet(prevIds[itPrev]);
	}
}

void AIManager::injectPeopleInZone(int idZone) {
	for (int i = 0; i < town.zones[idZone]->linkedStreets.size(); i++) {	
		town.wakeUpStreet(town.zones[idZone]->linkedStreets[i]->getId());
	}
}

void AIManager::incrementDeads() { 
	++deadPeople;
	PostProcessManager.incrementDeadsEffect();
}

void AIManager::onStartElement(GameObject *go, const std::string &elem, MKeyValue &atts ) {
	string IAType = atts["type"];
	bool has_navmesh_agent = atts.getBool("navmesh_agent", false);
	bool has_dynamic_obstacle_avoidance = atts.getBool("dynamic_obstacle_avoidance", false);

	if (ias.find(IAType) == ias.end()) {
		ias[IAType] = vector<GameObject *>();
	}
	ias[IAType].push_back(go);	//Save pointer to gameobject with IA.

	//Assign the selfcomponent by IA
	if (IAType.compare("vagabondFloor")== 0) {
		CVagabond * v = go->add<CVagabond>();
		v->setTypeVagabond(atts.getString("idle", "0"));
		v->Init();
	} else if (IAType.compare("guardian")==0) {
		CGuardian *g = go->add<CGuardian>();
		g->has_dynamic_obstacle_avoidance = has_dynamic_obstacle_avoidance;
		g->has_navmesh_agent = has_navmesh_agent;
		g->Init();
		g->WakeUp();
	} else if (IAType.compare("vagabondFire")== 0) {
		CVagabondFire *v = go->add<CVagabondFire>();
		v->Init();
	} else if (IAType.compare("walkerRandom") == 0) {
		CWalkerRandom *w = go->add<CWalkerRandom>();
		w->Init();
	} else if (IAType.compare("talker") == 0) {
		CTalker *t = go->add<CTalker>();
		t->Init();
		t->setNext(atts.getString("next", go->getName()));
	} else if (IAType.compare("market") == 0) {
		CPeopleMarket *m = go->add<CPeopleMarket>();
		m->Init();
		m->setPath(atts.getInt("idPath", -1), atts.getInt("vtxBegin", -1));
	} else if (IAType.compare("drunk") == 0) {
		CDrunk *d = go->add<CDrunk>();
		d->Init();
		d->setPath(atts.getInt("idPath", -1), atts.getInt("vtxBegin", -1));
	} else if (IAType.compare("observator") == 0) {
		CObservator *o = go->add<CObservator>();
		o->Init();
	} else if (IAType.compare("banker") == 0) {
		CBanker *b = go->add<CBanker>();
		b->Init();
		b->setPath(atts.getInt("idPath", -1));
		b->setHudTimer(atts.getInt("hud_timer", 0));
	} else if (IAType.compare("womanPriest") == 0) {
		CWomanPriest *w = go->add<CWomanPriest>();
		w->Init();
		w->setPath(atts.getInt("idPath", -1));
	}
	else if (IAType.compare("cardenal") == 0)
	{
		CCardenal *c = go->add<CCardenal>();
		c->Init();
		c->setPath(atts.getInt("idPath", -1));
		c->setHudTimer(atts.getInt("hud_timer", 0));
		c->has_dynamic_obstacle_avoidance = has_dynamic_obstacle_avoidance;
		c->has_navmesh_agent = has_navmesh_agent;

	} else if (IAType.compare("mayor") == 0) {
		CMayor *m = go->add<CMayor>();
		m->Init();
		m->setPath(atts.getInt("idPath", -1));
		m->has_dynamic_obstacle_avoidance = has_dynamic_obstacle_avoidance;
		m->has_navmesh_agent = has_navmesh_agent;
		m->setHudTimer(atts.getInt("hud_timer", 0));

	} else if (IAType.compare("obelisc") == 0) {
		CObelisc *o = go->add<CObelisc>();
		o->Init();
	}

	if (has_navmesh_agent)
		go->add<CNavmeshAgent>();

	if (has_dynamic_obstacle_avoidance)
		go->add<CObstacleAvoidance>();

}

void AIManager::restorePolice() {
	vector<GameObject*> guardians = getIAByType("guardian");
	vector<GameObject*>::const_iterator it = guardians.begin();
	while(it != guardians.end()) {
		CGuardian *comp_guardian = (*it)->get<CGuardian>();
		comp_guardian->Reset();
		++it;
	}
}



void AIManager::restore() {
	GameObject* go;
	switch(curTarget) {
		case TARGET_MAYOR:
			go = GameObjectManager.get("Mayor");
			if (go)	{
				if (go->has<CMayor>() && go->get<CMayor>()->isActive())	{
					go->get<CMayor>()->restore();
				}
			}
			break;
		case TARGET_CARDINAL:
			go = GameObjectManager.get("Cardenal");
			if (go) {
				if (go->has<CCardenal>() && go->get<CCardenal>()->isActive()) {
					go->get<CCardenal>()->restore();
				}
			}

			go = GameObjectManager.get("WomanPriest");
			if (go){
				if (go->has<CWomanPriest>()){
					go->get<CWomanPriest>()->restore();
				}
			}
			break;
		default:
			break;
	}

	usePlayerQTE(false);
	deadPeople = 0;
}

void AIManager::usePlayerQTE(bool useQTE) {
	aiPlayer->useQTE = useQTE;
}


void AIManager::activateDistraction(XMVECTOR posGo) {
	if (posObservation.size() != 0) {
		posObservation.clear();
		posObservation.reserve(NUM_OBSERVATORS+1);
	}
	float angle_rad = XM_2PI/NUM_OBSERVATORS;
	posObservation.push_back(ObservationPos(posGo, true));	//Center to stun position

	// Generate points arround de stun position
	for (int i = 0; i < NUM_OBSERVATORS; ++i) {
		XMVECTOR pos = posGo+XMVectorSet(RAD_OBSERVATORS*sin(angle_rad*i), 0.f, RAD_OBSERVATORS*cos(angle_rad*i), 0.f);
		posObservation.push_back(ObservationPos(pos, false));
	}
	sbb["stun"] = 1;
}


bool AIManager::getFreePosDistraction(XMVECTOR pos, XMVECTOR &posDistraction) {

	if (distEuclidean2(pos, posObservation[0].pos) < DIST_DISTRACTION_2) {
		int idx;
		float distMin = FLT_MAX;
		//Search the nearest position
		for (int i = 1; i < posObservation.size(); i++) {	// i == 0 is a center
			float distance = distEuclidean2(pos, posObservation[i].pos);
			if (distance < distMin) {
				idx = i;
				distMin = distance;
			}
		}

		//first the nearest
		if (!posObservation[idx].busy) {
			posDistraction = posObservation[idx].pos;
			posObservation[idx].busy = true;
			return true;
		} else {		//if not is possible see the next
			idx--;
			if (idx==0) idx = NUM_OBSERVATORS;

			if (!posObservation[idx].busy) {
				posDistraction = posObservation[idx].pos;
				posObservation[idx].busy = true;
				return true;
			}

			idx += 2;
			if (idx > NUM_OBSERVATORS) idx = 1;
			if (!posObservation[idx].busy) {
				posDistraction = posObservation[idx].pos;
				posObservation[idx].busy = true;
				return true;
			}
		}

		//If not is possible retrun false
		return false;
	}

	return false;
}

void AIManager::desactivateDistraction() { 
	sbb["stun"] = 0; 
	posObservation.clear();
	posObservation.reserve(NUM_OBSERVATORS+1);
}


void AIManager::UpdatePlayer() {
	if (aiPlayer) {
		aiPlayer->Recalc();
		aiPlayer->Update();
	}
}

