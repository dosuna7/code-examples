#ifndef INC_COMP_WALKER_RANDOM_H
#define INC_COMP_WALKER_RANDOM_H

#include "mcv_platform.h"
#include "comp_ai_sm.h"
#include "street_graph.h"

class CWalkerRandom: public AI_SM {
	Crossing *curCross;
	Street *curStreet;
	Track *curTrack;
	XMVECTOR crossPoint;

	float timer;
	bool scared;
	bool distraction;
	bool inCross;

	XMVECTOR bornPos;
	XMVECTOR bornQuat;
	XMVECTOR collidingPos;
	XMVECTOR distractionPos;

	string walkAnim;
	string turnAnim;

	//STATES
	void walkState();
	void inNodeState();
	void goPointState();
	void agonyState();
	void deadState();
	void scaredRunState();
	void scaredStopState();
	void returnPosState();
	void stunState();
	void goToDistractionState();
	void distractionState();

	//CHECKS
	void checkTurning();
	void checkDistraction();

	void lifeIdle();
	void lifeWalk();
	
public:
	CWalkerRandom() { }

	void setCross(Crossing *cross) { curCross = cross; }
	void setStreet(Street *street) { curStreet = street; }
	void setTrack(Track *track) { curTrack = track; }
	bool isSleeping() { return sleeping; }

	void Init();
	void update(float dt);
	void reappear();

	void activateScared(float timeFear, XMVECTOR fearPos);
	void disableScared();


	// MENSAJES
	static void registerMsgs();
	void renderDebug3D();

	void onIAStunned(const TMsg &msg);





};

#endif