#ifndef INC_COMPONENT_IA_SM_H
#define INC_COMPONENT_IA_SM_H

#include "mcv_platform.h"
#include "comp_ai.h"
#include "aicontroller.h"


class AI_SM: public IA_BASE, public AIController
{
protected:
	string prevState;

	void life();


public:
	AI_SM();
	void update(float dt);

};




#endif
