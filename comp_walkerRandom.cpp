#include "mcv_platform.h"
#include "comp_walkerRandom.h"
#include "comp_skeleton.h"
#include "comp_transform.h"
#include "AIConstants.h"
#include "comp_life.h"
#include "go_constants.h"
#include "ai_manager.h"
#include "camera_manager.h"
#include "comp_dissolve.h"
#include "rendering_manager.h"


DECL_COMP_MANAGER( CWalkerRandom, "ia_walkerRandom" );

void CWalkerRandom::registerMsgs( ) {
	SUBSCRIBE( CWalkerRandom, MSG_IA_STUNNED, onIAStunned);
}

void CWalkerRandom::onIAStunned(const TMsg &msg) { // STUN event

	if (!dead && !sleeping && animation != IA_BASE::AGONY && getCurrentState().compare("dead") != 0) {
		prevState = getCurrentState();	
		string s = Random::RandomAnimation("stun", 0, 1);
		owner->get<CSkeleton>()->changeCycle(s);
		animation = IA_BASE::STUN;
		timer = msg.stun_data.stun_time;
		ChangeState("stun");
	}	
}

void CWalkerRandom::activateScared(float timeFear, XMVECTOR fearPos) {
	CTransform *t = owner->get<CTransform>();

	if (isActive() && !scared && animation != IA_BASE::AGONY && distEuclidean2(fearPos, t->getPosition()) < FEAR_AREA_SCARED_2
		&& getCurrentState().compare("dead") != 0 && animation != IA_BASE::STUN) {
		if (getCurrentState().compare("returnPos") != 0) {
			bornPos = t->getPosition();
			bornQuat = t->getRotation();
			prevState = getCurrentState();
		}


		XMVECTOR vRun = t->getPosition() - fearPos;
		t->setYaw(atan2(XMVectorGetX(vRun), XMVectorGetZ(vRun)));

		collidingPos = getCollingPos(timeFear);
		if (XMVectorGetW(collidingPos) == -1.f || distEuclidean2(collidingPos, t->getPosition()) < 1.f)  {
			owner->get<CSkeleton>()->changeCycle(Random::RandomAnimation("stunWall", 0, 1));
			animation = IA_BASE::STOP;
			ChangeState("scaredStop");
		} else {
			owner->get<CSkeleton>()->changeCycle("runScared");
			animation = IA_BASE::RUN;
			ChangeState("scaredRun");
		}
		scared = true;		
	}
}

void CWalkerRandom::Init() {
	AddState("walk",		(statehandler)&CWalkerRandom::walkState);
	AddState("nextTrack",	(statehandler)&CWalkerRandom::inNodeState);
	AddState("goToPoint",	(statehandler)&CWalkerRandom::goPointState);
	AddState("dead",		(statehandler)&CWalkerRandom::deadState);
	//Scared
	AddState("scaredRun",	(statehandler)&CWalkerRandom::scaredRunState);
	AddState("scaredStop",	(statehandler)&CWalkerRandom::scaredStopState);
	AddState("returnPos",	(statehandler)&CWalkerRandom::returnPosState);
	//Stun
	AddState("stun",		(statehandler)&CWalkerRandom::stunState);
	AddState("goDistraction",(statehandler)&CWalkerRandom::goToDistractionState);
	AddState("distraction",	(statehandler)&CWalkerRandom::distractionState);

	int typeAnim = Random::IntegerRandom(0, NUM_WALK_ANIMATIONS-1);
	assert(typeAnim != NUM_WALK_ANIMATIONS);
	
	walkAnim = "walk" + int2string(typeAnim);
	turnAnim = "turn" + int2string(typeAnim);

	owner->get<CSkeleton>()->playCycle(walkAnim);
	animation = IA_BASE::WALK;
	timer = 0.f;
	scared = false;
	inCross = false;
	distraction = false;
	ChangeState("walk");
	StandBy();
}


void CWalkerRandom::reappear() {
	WakeUp();
	owner->get<CLife>()->resetLife();
	owner->get<CSkeleton>()->changeCycle(walkAnim);
	animation = IA_BASE::WALK;
	ChangeState("walk");
}

void CWalkerRandom::walkState() {

	if (animation == IA_BASE::TURN && owner->get<CSkeleton>()->model->getMixer()->isFinishedAnimation()) {	//turn animation is finished
			animation = IA_BASE::WALK;
			owner->get<CSkeleton>()->playCycle(walkAnim);
	}

	advanceWalk();

	if (curCross->isInside(owner->get<CTransform>()->getPosition())) {
		ChangeState("nextTrack");
		return;
	}

	if (!distraction && aiManager.isStunning())
		checkDistraction();

	checkTurning();
	lifeWalk();
}

void CWalkerRandom::inNodeState() {
	if (animation == IA_BASE::TURN && owner->get<CSkeleton>()->model->getMixer()->isFinishedAnimation()) {	//turn animation is finished
			animation = IA_BASE::WALK;
			owner->get<CSkeleton>()->playCycle(walkAnim);
	}

	if (curCross->linked_streets.size() == 1) {	//If don't have exit return for the same street
		if (curStreet->crossA == curCross) {
			curCross = curStreet->crossB;
		} else if (curStreet->crossB == curCross) {
			curCross = curStreet->crossA;
		}
		turnAngle(XM_PI);

		ChangeState("walk");
		return;
	} else {	
		//Select a random street
		aiManager.town.getIntelligenceStreet(owner, &curCross, &curStreet, &curTrack, crossPoint, aiManager.getZonePlayer());
		if (XMVectorGetW(crossPoint) == 0.f) {	//if are secant
			inCross = true;
			ChangeState("goToPoint");
		} else 
			ChangeState("walk");
	}
	if (!distraction && aiManager.isStunning())
		checkDistraction();

	checkTurning();
	lifeWalk();
}

void CWalkerRandom::goPointState() {
	if (animation == IA_BASE::TURN && owner->get<CSkeleton>()->model->getMixer()->isFinishedAnimation()) {
			animation = IA_BASE::WALK;
			owner->get<CSkeleton>()->playCycle(walkAnim);
	}

	turnTo(crossPoint);
	advanceWalk();
	if (distEuclidean2(owner->get<CTransform>()->getPosition(), crossPoint) < 0.05f) {
		XMVECTOR vTrack = curTrack->getVector();

		if (curCross == curStreet->crossA)
			vTrack = -vTrack;

		owner->get<CTransform>()->setYaw(atan2(XMVectorGetX(vTrack), XMVectorGetZ(vTrack)));
		inCross = false;
		ChangeState("walk");
	} 

	if (!distraction && aiManager.isStunning())
		checkDistraction();

	checkTurning();
	lifeWalk();
}

void CWalkerRandom::deadState() {
	bool acabat = owner->get<CSkeleton>()->model->getMixer()->isFinishedAnimation();
	
	if (acabat) {
		dead = true;
		StandBy();
	}

	dissolvePerson(acabat);
}

void CWalkerRandom::scaredRunState() {
	advanceRun();
	if (scared && arrivePoint(collidingPos) && animation != AGONY)
	{
		owner->get<CSkeleton>()->changeCycle(Random::RandomAnimation("stunWall", 0, 1));
		animation = IA_BASE::STOP;
		ChangeState("scaredStop");
	} 
	else if (!scared)
	{
		owner->get<CSkeleton>()->changeCycle(walkAnim);
		animation = IA_BASE::WALK;
		ChangeState("returnPos");
	} 
	else
	{
		lifeWalk();
	}
}

void CWalkerRandom::scaredStopState() {
	if (scared) {
		lifeIdle();
	} else {
		owner->get<CSkeleton>()->changeCycle(walkAnim);
		animation = IA_BASE::WALK;
		ChangeState("returnPos");
		lifeWalk();
	}
		
}

void CWalkerRandom::returnPosState() {

	advanceToPoint(SPEED_WALK, ANGULAR_SPEED_CHASE_POLICE, bornPos);

	if (arrivePoint(bornPos)) {
		bornPos = XMVectorSetW(bornPos, 0.f);
		owner->get<CTransform>()->setRotation(bornQuat);
		owner->get<CSkeleton>()->changeCycle(walkAnim);
		animation = IA_BASE::WALK;
		ChangeState(prevState);
	}	
	lifeWalk();
}

void CWalkerRandom::stunState() {
	timer -= Time.getDeltaTime();
	if (timer <= 0.f) {
		timer = 0.f;
		aiManager.desactivateDistraction();
		owner->get<CSkeleton>()->changeCycle(walkAnim);
		animation = IA_BASE::WALK;
		ChangeState(prevState);
	}
}

void CWalkerRandom::goToDistractionState() {
	advanceToPoint(SPEED_WALK, ANGULAR_SPEED_CHASE_POLICE, distractionPos );
	if (arrivePoint(distractionPos) && animation != AGONY) {
		owner->get<CSkeleton>()->changeCycle("distraction");
		turnTo(aiManager.getCenterDistraction());
		animation = IA_BASE::IDLE_DISTRACTION;
		ChangeState("distraction");
	}
	if (!aiManager.isStunning()) {
		distraction = false;		
		ChangeState("returnPos");
	}
	lifeWalk();
}

void CWalkerRandom::distractionState() {
	if (!aiManager.isStunning()) {
		distraction = false;
		owner->get<CSkeleton>()->changeCycle(walkAnim);
		animation = IA_BASE::WALK;
		ChangeState("returnPos");
	}
	lifeIdle();
}


void CWalkerRandom::disableScared() {
	scared = false;
}

void CWalkerRandom::checkTurning() {
	if (animation == IA_BASE::WALK || animation == IA_BASE::AGONY) {
		bool collisioning = false;
		if (inCross) {
			for (int i = 0; i < curCross->linked_streets.size() && !collisioning; i++) {
				for (int j = 0; j < curCross->linked_streets[i]->tracks.size() && !collisioning; j++) {
					collisioning = curCross->linked_streets[i]->tracks[j].goAreCollisioning(owner, DIST_COLLISION);
				}
			}
		} else {
			collisioning = curTrack->goAreCollisioning(owner, DIST_COLLISION);
		}

		if (animation != IA_BASE::TURN && collisioning) {
			owner->get<CSkeleton>()->stopCycle();
			owner->get<CSkeleton>()->playAnimation(turnAnim);
			animation = IA_BASE::TURN;
		}

	}
}

void CWalkerRandom::checkDistraction() {
	if (aiManager.getFreePosDistraction(owner->get<CTransform>()->getPosition(), distractionPos)) {
		bornPos = owner->get<CTransform>()->getPosition();
		bornQuat = owner->get<CTransform>()->getRotation();
		prevState = getCurrentState();
		distraction = true;
		ChangeState("goDistraction");
	}	
	

}

void CWalkerRandom::update(float dt) {
	if (!sleeping) {
		Recalc();
	}
}

#include "draw_utils.h"
void CWalkerRandom::renderDebug3D() {
	if (distEuclidean2(owner->get<CTransform>()->getPosition(), CameraManager.getActiveCamera().getPosition()) < 300.f) {
		DrawUtils::dbg_font.print3Df( owner->get<CTransform>()->getPosition()
			, "%s\n %s", owner->getName(), getCurrentState().c_str());

		for (int i = 0; i < aiManager.posObservation.size(); i++) {
			XMMATRIX mat = XMMatrixTranslationFromVector( aiManager.posObservation[i].pos);
			DrawUtils::setWorldMatrix(mat);
			DrawUtils::drawAxis( );
		}
	}
}


void CWalkerRandom::lifeWalk() {
	if (animation == IA_BASE::AGONY && owner->get<CSkeleton>()->getCurrentAnimation()[5] == 'I') {
		string agonyName = "agonyWalk" + string(&owner->get<CSkeleton>()->getCurrentAnimation().back());
		owner->get<CSkeleton>()->changeCycle(agonyName);
	}
	if (isInFear()) {
		float lifeBefore = owner->get<CLife>()->getLife();
		decreaseLife();
		float life = owner->get<CLife>()->getLife();

		if (life < LIFE_TO_AGONY_1) {
			if (lifeBefore >= LIFE_TO_AGONY_1 && life < LIFE_TO_AGONY_1) {
				prevState = getCurrentState();
				owner->get<CSkeleton>()->changeCycle("agonyWalk1");
				animation = IA_BASE::AGONY;
			} else if (lifeBefore >= LIFE_TO_AGONY_2 && life < LIFE_TO_AGONY_2) {
				owner->get<CSkeleton>()->changeCycle("agonyWalk2");
			} else if (lifeBefore >= LIFE_TO_AGONY_3 && life < LIFE_TO_AGONY_3) { 
				owner->get<CSkeleton>()->changeCycle("agonyWalk3");
			} else if (life <= 0.f && animation != IA_BASE::DEAD) {
				owner->get<CSkeleton>()->stopCycle();
				owner->get<CSkeleton>()->playAnimation("deadWalk");
				animation = IA_BASE::DEAD;
				aiManager.incrementDeads();
				ChangeState("dead");
			}
		}
	} else {
		float lifeBefore = owner->get<CLife>()->getLife();
		increaseLife();
		float life = owner->get<CLife>()->getLife();
		if (animation == IA_BASE::AGONY) {
			if (life > LIFE_TO_AGONY_1) {
				owner->get<CSkeleton>()->changeCycle(walkAnim);
				animation = IA_BASE::WALK;
			} else if (lifeBefore <= LIFE_TO_AGONY_2 && life > LIFE_TO_AGONY_2) {
				owner->get<CSkeleton>()->changeCycle("agonyWalk1");
			} else if (lifeBefore <= LIFE_TO_AGONY_3 && life > LIFE_TO_AGONY_3) {
				owner->get<CSkeleton>()->changeCycle("agonyWalk2");
			}
		}
	}
}

void CWalkerRandom::lifeIdle() {
	if (animation == IA_BASE::AGONY && owner->get<CSkeleton>()->getCurrentAnimation()[5] == 'W') {
		string agonyName = "agonyIdle" + string(&owner->get<CSkeleton>()->getCurrentAnimation().back());
		owner->get<CSkeleton>()->changeCycle(agonyName);
	}
	if (isInFear()) {
		float lifeBefore = owner->get<CLife>()->getLife();
		decreaseLife();
		float life = owner->get<CLife>()->getLife();

		if (life < LIFE_TO_AGONY_1) {
			if (lifeBefore >= LIFE_TO_AGONY_1 && life < LIFE_TO_AGONY_1) {
				prevState = getCurrentState();
				owner->get<CSkeleton>()->changeCycle("agonyIdle1");
				animation = IA_BASE::AGONY;
			} else if (lifeBefore >= LIFE_TO_AGONY_2 && life < LIFE_TO_AGONY_2) {
				owner->get<CSkeleton>()->changeCycle("agonyIdle2");
			} else if (lifeBefore >= LIFE_TO_AGONY_3 && life < LIFE_TO_AGONY_3) {
				owner->get<CSkeleton>()->changeCycle("agonyIdle3");
			} else if (life <= 0.f && animation != IA_BASE::DEAD) {
				owner->get<CSkeleton>()->stopCycle();
				owner->get<CSkeleton>()->playAnimation("deadIdle");
				animation = IA_BASE::DEAD;
				aiManager.incrementDeads();
				ChangeState("dead");
			} 
		}
	} else {
		float lifeBefore = owner->get<CLife>()->getLife();
		increaseLife();
		float life = owner->get<CLife>()->getLife();
		if (animation == IA_BASE::AGONY) {
			if (life > LIFE_TO_AGONY_1) {
				owner->get<CSkeleton>()->changeCycle("idle");
				animation = IA_BASE::IDLE;
			} else if (lifeBefore <= LIFE_TO_AGONY_2 && life > LIFE_TO_AGONY_2) {
				owner->get<CSkeleton>()->changeCycle("agonyIdle1");
			} else if (lifeBefore <= LIFE_TO_AGONY_3 && life > LIFE_TO_AGONY_3) {
				owner->get<CSkeleton>()->changeCycle("agonyIdle2");
			}
		}
	}
}