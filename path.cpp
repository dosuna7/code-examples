#include "mcv_platform.h"
#include "path.h"
#include "draw_utils.h"

Waypoint::Waypoint(XMVECTOR aquat, XMVECTOR apos, float atime) {
	quat = aquat;
	pos = apos;
	time = atime;
}

Path::Path(int aid, bool acyclic) {
	id = aid;
	cyclic = acyclic;
	current = 0;
	incrementar = true;
}

XMVECTOR Path::startPath() { 
	current = 0;
	return waypoints[0].pos;
}

void Path::setCurrent(int numWaypoint) {
	assert(numWaypoint < waypoints.size() && numWaypoint >= 0);
	current = numWaypoint;

}

void Path::nextWaypoint() {
	if (cyclic) {
		current = (current+1) % waypoints.size();
	} else {
		if (incrementar) {	
			current++;
			if (current == waypoints.size()-1) {	
				incrementar = false;
			}
		} else {	
			current--;
			if (current == 0)	
				incrementar = true;
		}
	}
}

bool Path::arriveFinalPath() {
	return current == waypoints.size()-1;
}

bool Path::isExtreme() {
	return current == 0 || current == waypoints.size()-1;
}

void Path::changeIncrement() {
	incrementar = !incrementar;
}

void Path::setIncrement(bool increment) {
	incrementar = increment;
}