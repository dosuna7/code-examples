#include "mcv_platform.h"
#include "comp_cardenal.h"
#include "AIConstants.h"
#include "ai_manager.h"
#include "comp_skeleton.h"
#include "go_constants.h"
#include "ai_path_manager.h"
#include "comp_life.h"
#include "logic_manager.h"
#include "comp_trigger_sphere.h"
#include "ui_manager.h"
#include "logic_constants.h"
#include "comp_sound_systems.h"
#include "comp_transform.h"
#include "comp_navmesh_agent.h"
#include "comp_obstacle_avoidance.h"

DECL_COMP_MANAGER( CCardenal, "ia_cardenal" );

void CCardenal::registerMsgs( )
{ 
	SUBSCRIBE( CCardenal, MSG_APPEAR_CARDENAL, msgWakeUp);
	SUBSCRIBE( CCardenal, MSG_WALK_CARDENAL, goChurch);
	SUBSCRIBE( CCardenal, MSG_IA_STUNNED, stun);
	SUBSCRIBE( CCardenal, MSG_IA_CHASE, onIAChase );
}

void CCardenal::setPath(int id) {
	assert(id != -1);
	path = aiPathManager.getPath(id);
}

void CCardenal::goChurch(const TMsg &msg) {
	owner->get<CSkeleton>()->changeCycle("run");
	animation = IA_BASE::RUN;
	ChangeState("chase");
}


void CCardenal::stun(const TMsg &msg) {
	string s = Random::RandomAnimation("stun", 0, 1);
	owner->get<CSkeleton>()->changeCycle(s);
	animation = IA_BASE::STUN;
	timer = msg.stun_data.stun_time;
	ChangeState("stun");
}

void CCardenal::msgWakeUp(const TMsg &msg) {
	WakeUp();
	owner->send(TMsg::createPhysicsMove(owner->get<CTransform>()->getPosition()));
	owner->get<CSkeleton>()->playCycle("idle_talk");
	animation = IA_BASE::TALK;
	ChangeState("speak");
	UIManager.setTimerHUD(hud_timer);
	UIManager.showTimerHUD();
	UIManager.setTargetToKillHUD(DeadTargets::TARGET_CARDINAL + 2);

}

void CCardenal::Init()
{
	AddState("speak",		(statehandler)&CCardenal::speakState);
	AddState("walk",		(statehandler)&CCardenal::walkState);
	AddState("stun",		(statehandler)&CCardenal::stunState);
	AddState("chase",		(statehandler)&CCardenal::chaseState);
	AddState("endStun",		(statehandler)&CCardenal::endStunState);
	AddState("finalPath",	(statehandler)&CCardenal::finalPathState);	
	AddState("enter_knocking_door",		(statehandler)&CCardenal::enterKnockingDoorState);
	AddState("enter_dead_finisher",		(statehandler)&CCardenal::enterFinisherDeadState);
	AddState("dead_finisher",		(statehandler)&CCardenal::finisherDeadState);
	AddState("exit_dead_finisher",		(statehandler)&CCardenal::exitFinisherDeadState);

	initPos = owner->get<CTransform>()->getPosition();
	initQuat = owner->get<CTransform>()->getRotation();
	transform = owner->get<CTransform>();

	if (owner->has<CSphereTrigger>()){
		owner->get<CSphereTrigger>()->enabled = false;
	}

	acc_dt = 0;
	dead_animation_time = 7;
	shutdown_timer = IA_TIMERS::SHUTDOWN_TIMER;

	dead				= false;
	chase				= false;
	stunned				= false;
	was_chasing			= false;
	return_born_pos		= false;

	Sleep();
}

void CCardenal::speakState() { }

#include "comp_trigger_sphere.h"
void CCardenal::walkState() {
	XMVECTOR waypoint = path->getPosWaypoint();
	advanceToPoint(SPEED_WALK, ANGULAR_SPEED_WALK, waypoint);
	if (arrivePoint(waypoint)) {
		if (path->arriveFinalPath()) {
			timerQTE = TIME_QTE;
			owner->get<CSkeleton>()->changeCycle("idle_talk");
			animation = IA_BASE::TALK;
			if (owner->has<CSphereTrigger>()){
				owner->get<CSphereTrigger>()->enabled = true;
				owner->get<CSphereTrigger>()->updatePos(owner->get<CTransform>()->getPosition());
			}
			ChangeState("finalPath");
			return;
		}
		path->nextWaypoint();
	}
}

void CCardenal::stunState() {
	if (timer > 0.f) {
		timer -= Time.getDeltaTime();
	} else {
		ChangeState("endStun");
	}
}

void CCardenal::endStunState() {
	if (path->arriveFinalPath()) {	//Big church
		owner->get<CSkeleton>()->changeCycle("idle_talk");
		animation = IA_BASE::TALK;
		ChangeState("finalPath");
	}  else if (path->isExtreme()) {	//little church
		owner->get<CSkeleton>()->changeCycle("idle_talk");
		animation = IA_BASE::TALK;
		ChangeState("speak");
	} else {
		owner->get<CSkeleton>()->changeCycle("walk");
		animation = IA_BASE::WALK;
		ChangeState("walk");
	}
}

void CCardenal::finalPathState() {
	if (timerQTE > 0.f) {
		timerQTE -= Time.getDeltaTime();
	} else {
		LogicManager.logicGame->forceLose();
		dead = true;
	}
}

void CCardenal::qteState() {
	if (owner->has<CSphereTrigger>()){
		owner->get<CSphereTrigger>()->enabled = false;
	}
}


void CCardenal::enterKnockingDoorState()
{
	if (animation != IA_BASE::KNOCKING_DOOR)
	{
		owner->get<CSkeleton>()->changeCycle("knocking_door");
		animation = IA_BASE::KNOCKING_DOOR;
	}
}

/*
	Finisher
*/
void CCardenal::enterFinisherDeadState()
{
	owner->get<CSkeleton>()->stopCycle();
	owner->get<CSkeleton>()->playAnimation("finisher_death_static", true);
	ChangeState("dead_finisher");
	acc_dt = 0;
	UIManager.hideTimerHUD();
	owner->get<CCompSoundSystems>()->play("Estrangular");
}

void CCardenal::finisherDeadState() {
	acc_dt += Time.deltaTime;
	if (acc_dt > dead_animation_time) {
		ChangeState("exit_dead_finisher");
		acc_dt = 0;
		owner->send(TMsg::createDissolveMSG(0.5));
		owner->setCullingState(false);
		UIManager.setKilledTargetHUD((int)DeadTargets::TARGET_CARDINAL + 2);
	}

	CTransform *myTransform = owner->get<CTransform>();
	XMVECTOR target_position = aiManager.getPosPlayer();
	float yaw_difference = getAngleDifferenceTo(myTransform->getPosition(), myTransform->getYaw(), target_position);
	myTransform->rotateYaw(yaw_difference * deg2rad(90) * Time.deltaTime * 5);
}

void CCardenal::exitFinisherDeadState() {
	acc_dt += Time.deltaTime;
	if (acc_dt > shutdown_timer) {
		dead = true;
		aiManager.killTarget();
		owner->setCullingState(false);
		StandBy();
	}
}


void CCardenal::update(float dt) {
	PROFILE_FUNC();
	if (!sleeping && !dead) {
		Recalc();
	}
}

void CCardenal::restore(){
	
	owner->get<CTransform>()->setPosition(initPos);
	owner->get<CTransform>()->setRotation(initQuat);
	owner->get<CLife>()->resetLife();
	path->setCurrent(0);

	if (owner->has<CSphereTrigger>()){
		owner->get<CSphereTrigger>()->enabled = false;
	}

	WakeUp();
	owner->send(TMsg::createPhysicsMove(owner->get<CTransform>()->getPosition()));
	owner->get<CSkeleton>()->playCycle("idle_talk");
	animation = IA_BASE::TALK;
	ChangeState("speak");
}

//-------------------------------------------------------
//					CHASE
//-------------------------------------------------------
void CCardenal::onIAChase(const TMsg &msg) {
	if (!stunned && !dead){
		chase = true;
		was_chasing = true;
		ChangeState("chase");
	}
}

void CCardenal::chaseState() {
	XMVECTOR modified_distance;
	XMVECTOR modified_pos;
	XMVECTOR avoidance_direction;
	XMVECTOR final_direction;
	XMVECTOR direction_to_computed_position;
	float weight = 0;
	bool modified = false;

	if (animation != IA_BASE::CHASE) {
		owner->get<CSkeleton>()->changeCycle("run");
		animation = IA_BASE::CHASE;
	}

	float threshold = 2 * IA_NAVMESH_AGENT::THRESHOLD;

	XMVECTOR destiny = aiManager.getChurchPosition();
	XMVECTOR origin = transform->getPosition();

	CNavmeshAgent *nva = owner->get<CNavmeshAgent>();
 	XMVECTOR computedPathPosition = nva->getTargetPathPoint(origin, destiny, threshold);
	direction_to_computed_position = XMVector3NormalizeEst(computedPathPosition - transform->getPosition());

	if (has_dynamic_obstacle_avoidance) {
		weight = owner->get<CObstacleAvoidance>()->calculateAvoidanceForce(transform, avoidance_direction, computedPathPosition, PhysicsConstants::CollisionGroups::CITIZEN);
	}

	final_direction = direction_to_computed_position * (1 - weight) + avoidance_direction * weight;
	final_direction = XMVector3Normalize(final_direction);
	final_direction = XMVectorSetW(final_direction, 1);

	float yaw_difference = getAngleDifferenceTo(transform->getPosition(), transform->getYaw(), transform->getPosition() + final_direction);
	
	float linearSpeed = IA_VELOCITIES::CHASE_SPEED;
	transform->rotateYaw(yaw_difference * deg2rad(90) * Time.deltaTime * 6);
	transform->translate(transform->getFront() * linearSpeed * 1 * Time.deltaTime);
	transform->owner->send(TMsg::createPhysicsMove(transform->pos));

	if (Mathf::Vector3Magnitude(transform->getPosition(), destiny) < 0.3f){
		chase = false;
		was_chasing = false;
		stunned = false;
		ChangeState("enter_knocking_door");
	}
}

